// Copyright (C) 2019 Richard Kunze <richard.kunze@mailbox.org>
//
// This file is part of Egon.
//
// Egon is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Egon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Egon.  If not, see <http://www.gnu.org/licenses/>.

package main

import "gitlab.com/richard.kunze/egon/cmd"

func main() {
	cmd.Execute()
}
