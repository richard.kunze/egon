# Configuration

Egon configuration is stored in a file named `egon-config.toml` in the top level directory of a Hugo site. 

_Note_: Although we use TOML for the examples here, 
the file type (and matching extension) of the `egon-config` file can be any type supported by [Viper].

[Viper]: https://github.com/spf13/viper