# Egon Tutorials

This series of tutorials shows how to use Egon to add interactive edit functions to a simple Hugo blog site. The series starts with the most basic functionality, subsequently adds more features, and finally ends with a production ready setup for a Hugo site with Egon edit functions.

0. ["The starting point" - A minimal Hugo blog](preliminary.md)
1. "First steps with Egon" - Add comments to blog posts
2. "I want to edit my comments" - Edit existing comments
3. "Wait: _Everybody_ can edit my blog comments?" - User authentication and authorization
4. "I want to create blog posts online, but nobody else may do so" - Advanced access control
5. "I can haz pics?" - Image and file upload
6. "Lets go public" - Production ready setup

The tutorials assume that you are already familiar with Hugo and Git, and that you use a Linux or Unix system to run Egon (Windows 10 with the "Windows Subsystem for Linux" should work as well, but is untested).
