# "The starting point" - A minimal Hugo blog

The starting point for our Egon tutorials is a minimal Hugo blog in the directory `egon-example`:

```sh
hugo new site egon-example
cd egon-example
git init
echo 'public' > .gitignore
git add .
git commit -m 'Create example site for Egon'
```
Since we want to concentrate on Egon and not on Hugo, we use a [simple Hugo theme](https://themes.gohugo.io/simple-hugo-theme/) to display our site:

```sh
git submodule add https://github.com/Xzya/simple-hugo-theme.git themes/simple
cat << _EOF_ >> config.toml
theme = "simple"
[menu]
  [[menu.main]]
    url = "/posts/"
    name = "Blog"
    weight = 1
_EOF_
git commit -a -m 'Add a simple theme'
```

For reasons we will see later, we want to use [page bundles](https://gohugo.io/content-management/page-bundles/) for our blog posts, so let's create a suitable archetype:

```sh
mkdir archetypes/posts
cat << _EOF_  > archetypes/posts/index.md
---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
---
This is an example blog post.
_EOF_
git add .
git commit -m 'Add archetype for blog posts'
```

Now, create some example blog posts:
```sh
hugo new posts/my-first-blog-post
hugo new posts/a-second-blog-post
git add .
git commit -m 'Add some example content'
```

And finally, run Hugo in server mode
```sh
hugo server
```
and open http://localhost:1313/ to look at the result.