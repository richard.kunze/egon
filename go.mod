module gitlab.com/richard.kunze/egon

go 1.11

require (
	github.com/mitchellh/go-homedir v1.1.0
	github.com/spf13/cobra v0.0.3
	github.com/spf13/viper v1.3.2
)
