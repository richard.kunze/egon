# A web API for managing Hugo content

Egon provides a simple web API for creating, changing and deleting 
pages and sections of web sites managed with [Hugo] and [Git].

It is primarily intended to provide simple edit functions 
&ndash; for example, a comment function for a blog, 
or image upload for an image gallery.
But it can be used as a building block for more advanced functionality as well.

## Quick Start

* Install [Hugo] and [Git]
* [Install Egon](#installation).
* [Configure a Hugo site for use with Egon](doc/configuration.md).
* Start the Egon API server for your Hugo site. Example:
  ```sh
  cd my-hugo-website
  egon server
  ```
  See the [Egon man page](doc/egon.md) or the output of `egon --help` for details.
* [Use the web API](doc/api.md) to create, update or delete content for your Hugo site.

See also the [tutorials](doc/tutorials/index.md) for an introduction to Egon.
## Installation

### Pre-Built

At the moment, no pre-built binaries are provided for Egon.

### From Source
#### Prerequisite Tools

* [Git](https://git-scm.com/)
* [Go (at least Go 1.11)](https://golang.org/dl/)

#### Fetch from GitLab and Build

Egon uses the Go Modules support available from Go 1.11. The easiest installation option is to clone Egon 
in a directory outside of `GOPATH`:

```sh
mkdir $HOME/src
cd $HOME/src
git clone https://gitlab.com/richard.kunze/egon.git
cd egon
go install
```

[Hugo]: https://gohugo.io 
[Git]: https://git-scm.com/